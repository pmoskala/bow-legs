import threading

import cv2
import numpy as np
import redis
from keras.models import load_model
from keras.models import model_from_json
from skimage import img_as_ubyte
from skimage import morphology, color
from skimage import transform
from os.path import abspath
r = redis.Redis(host='localhost', port=6379, db=0)



def IoU(y_true, y_pred):
    """Returns Intersection over Union score for ground truth and predicted masks."""
    assert y_true.dtype == bool and y_pred.dtype == bool
    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()
    intersection = np.logical_and(y_true_f, y_pred_f).sum()
    union = np.logical_or(y_true_f, y_pred_f).sum()
    return (intersection + 1) * 1. / (union + 1)

def Dice(y_true, y_pred):
    """Returns Dice Similarity Coefficient for ground truth and predicted masks."""
    assert y_true.dtype == bool and y_pred.dtype == bool
    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()
    intersection = np.logical_and(y_true_f, y_pred_f).sum()
    return (2. * intersection + 1.) / (y_true.sum() + y_pred.sum() + 1.)

def load_models():
    with open(abspath('./worker/models/model_bk.json'), 'r') as json_file:
        loaded_model_json = json_file.read()
        model = model_from_json(loaded_model_json)
        model.load_weights(abspath('./worker/models/trained_model.hdf5'))
        return model

def remove_small_regions(img, size):
    """Morphologically removes small (less than size) connected regions of 0s or 1s."""
    img = morphology.remove_small_objects(img, size)
    img = morphology.remove_small_holes(img, size)
    return img


def masked(img,  mask, alpha=1.):
    """Returns image with GT lung field outlined with red,
	predicted lung field filled with blue."""
    rows, cols = img.shape
    color_mask = np.zeros((rows, cols, 3))

    # boundary = morphology.dilation(gt, morphology.disk(3)) - gt
    # boundary = morphology.dilation(gt, morphology.disk(3)) ^ gt
    # boundary = morphology.dilation(gt, morphology.disk(1)) ^ gt

    color_mask[mask == 1] = [0, 0, 1]
    # color_mask[boundary == 1] = [1, 0, 0]
    img_color = np.dstack((img, img, img))

    img_hsv = color.rgb2hsv(img_color)
    color_mask_hsv = color.rgb2hsv(color_mask)

    img_hsv[..., 0] = color_mask_hsv[..., 0]
    img_hsv[..., 1] = color_mask_hsv[..., 1] * alpha

    img_masked = color.hsv2rgb(img_hsv)
    return img_masked

def binarize(m):
    pr = m > 0.5
    pr = remove_small_regions(pr, 0.005 * np.prod(m.shape))
    return pr


def predict(img, model):
    height, width = img.shape[:2]
    grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) if len(img.shape) == 3 else img
    img = transform.resize(grey[:, :], (512, 256), mode='constant')
    img = np.expand_dims(img, -1)
    img -= img.mean()
    img /= img.std()
    xx = img[:, :, :][None, ...]
    pred = model.predict(xx)[..., 0].reshape(img.shape[:2])

    # Binarize masks
    pr = pred > 0.5
    pr = remove_small_regions(pr, 0.005 * np.prod(pr.shape))

    pr_bin = img_as_ubyte(pr)
    pr_openned = morphology.opening(pr_bin)
    mask = cv2.resize(pr_openned, (width, height))
    masked_img = masked(grey,  mask > 0.5, alpha=0.5)
    img = cv2.cvtColor(img_as_ubyte(masked_img), cv2.COLOR_BGR2RGB)

    return img, mask

def make_mask():
    # r = redis.Redis(host='localhost', port=6379, db=0)
    sub = r.pubsub()
    sub.subscribe('queue')
    model = load_models()
    for task in sub.listen():
        path = task['data']
        if path != 1:
            decoded = path.decode("utf-8")
            img = cv2.imread(decoded, cv2.IMREAD_UNCHANGED)
            result, result_mask = predict(img, model)
            result_path = decoded.replace('.png', '_result.png')
            mask_path = decoded.replace('.png', '_mask.png')
            cv2.imwrite(result_path, result)
            cv2.imwrite(mask_path, result_mask)

def compare_mask():

    sub = r.pubsub()
    sub.subscribe('mask')
    for task in sub.listen():
        path = task['data']
        if path != 1:
            decoded = path.decode("utf-8")
            uuid = decoded.split('/')[-1].split("_")[0]
            mask = cv2.imread(decoded, cv2.IMREAD_UNCHANGED)
            predicted =  cv2.imread(decoded.replace('ground.png', 'mask.png'), cv2.IMREAD_UNCHANGED)
            mb = binarize(mask)
            pb = binarize(predicted)
            iou = IoU(mb, pb)
            dice = Dice(mb, pb)
            r.set(f'iou-{uuid}',iou )
            r.set(f'dice-{uuid}',dice )
            # result, result_mask = predict(img, model)
            # result_path = decoded.replace('.png', '_result.png')
            # mask_path = decoded.replace('.png', '_mask.png')
            # cv2.imwrite(result_path, result)
            # cv2.imwrite(mask_path, result_mask)


def main():
    t = threading.Thread(name='make-mask', target=make_mask)
    t1 = threading.Thread(name='compare-mask', target=compare_mask)
    t.start()
    t1.start()
    t.join()

if __name__ == '__main__':
    main()
