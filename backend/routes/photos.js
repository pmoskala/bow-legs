var express = require('express');
var redis = require('redis');
var multer = require('multer');
const {promisify} = require('util');
var router = express.Router();
const uuidv1 = require('uuid/v1');
const storage_P = '/Users/pmoskala/Agh/s9/cv/bow-legs/backend/public/images';
const redisCli = redis.createClient();
const getAsync = promisify(redisCli.get).bind(redisCli);
const publishCli = redis.createClient();
const PHOTO_QUEUE = 'queue';
const MASKS_QUEUE = 'mask';
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, storage_P)
    },
    filename: function (req, file, cb) {
        const filename = req.fileName ? req.fileName : uuidv1() + '.png';
        cb(null, filename)
    }
});
var upload = multer({storage: storage}).single('file');


router.post('/', function (req, res, next) {
    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }
        const path = req.file.path;
        publishCli.publish(PHOTO_QUEUE, path);

        const fileName = req.file.filename;
        return res.status(200).send(fileName)
    })
});

router.post('/:photoUuid', function (req, res, next) {
    const photoUuid = req.params.photoUuid;
    req.fileName = `${photoUuid}_ground.png`;

    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }
        const path = req.file.path;
        publishCli.publish(MASKS_QUEUE, path);

        const fileName = req.file.filename;
        return res.status(200).send(fileName)
    })
});

router.get('/stats/:photoUuid', function (req, res, next) {
    const photoUuid = req.params.photoUuid;
    Promise.all([
        getAsync(`iou-${photoUuid}`),
        getAsync(`dice-${photoUuid}`),
    ])
        .then(([iou, dice]) => {
            if (iou == null)
                return res.status(404).json({iou, dice})
            else {
                return res.status(200).json({iou, dice})
            }
        })
        .catch(err => {
            return res.status(500).json(err)
        });

});
module.exports = router;
