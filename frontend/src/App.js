import React from 'react';
import './App.css';
import axios from 'axios';
import Image from "react-graceful-image";
import retry from 'fetch-retry'

const BACKEND_API = 'http://0.0.0.0:3009';

class App extends React.Component {

    constructor() {
        super();

        this.state = {
            img: null,
            mask: null,
            maskGolden: null,
            out: null,
            uuid: null,
        }
    }

    sendImg(data) {
        const url = `${BACKEND_API}/photos`;
        return axios.post(url, data, {
            // receive two    parameter endpoint url ,form data
        })
    //y

    }

    sendMask(uuid, data) {
        const url = `${BACKEND_API}/photos/${uuid}`;
        return axios.post(url, data, {
            // receive two    parameter endpoint url ,form data
        })
    //y

    }

    getStats(uuid) {
        const url = `${BACKEND_API}/photos/stats/${uuid}`;
        return retry(url, {
            method: 'GET',
            retries: 10,
            retryDelay: 1000,
            retryOn: [404, 500]
        })
            .then(function(response) {
                return response.json();
            })
    }

    render() {
        return (
            <div className="App">
                <div className="form">
                    <label>Photo: </label>
                    <input type="file" name="file" onChange={event => {
                        console.log(event.target.files[0])
                        const img = event.target.files[0]
                        const data = new FormData()
                        data.append('file', img)
                        this.sendImg(data)
                            .then(res => {
                                const uuid = res.data.split('.png')[0];
                                const img = `${BACKEND_API}/images/${res.data}`;
                                const mask = `${BACKEND_API}/images/${uuid}_result.png`;
                                return this.setState({img, uuid, mask, out: null});
                            })
                    }}/>

                    <label>Mask: </label>
                    <input type="file" name="file" onChange={event => {
                        console.log(event.target.files[0]);
                        const img = event.target.files[0];
                        const data = new FormData();
                        if (this.state.uuid){
                            data.append('file', img);
                            this.sendMask(this.state.uuid, data)
                                .then(res => {
                                    const maskGolden = `${BACKEND_API}/images/${res.data}`;
                                    return this.setState({maskGolden, out: "Counting..."})

                                })
                                .then( () => this.getStats(this.state.uuid))
                                .then(({iou, dice}) => this.setState({out: `IOU: ${iou}\n dice: ${dice}`}));

                        }

                    }}/>


                </div>
                <div className="output">

                    <div className="images">
                        <div className="img-src">
                            {this.state.img ?
                                <div>
                                <h3>Source photo</h3>
                                    <Image src={`${this.state.img}`}/>
                                </div>
                                :
                            ''}
                        </div>
                        <div className="img-src">
                            {this.state.maskGolden ? <div>
                                    <h3>Mask</h3>
                                    <Image src={`${this.state.maskGolden}`} retry={{ count: 10, delay: 2 }}/>
                                </div>
                                : ''}
                        </div>
                        <div className="img-src">
                            {this.state.mask ? <div>
                                    <h3>Predicted mask</h3>
                                    <Image src={`${this.state.mask}`} retry={{ count: 10, delay: 2 }}/>
                                </div>
                                : ''}
                        </div>


                    </div>
                    <div className="text">
                        {this.state.out ? <h3>{this.state.out}</h3>: <h3>Please upload source file and mask, to get Intersection over Union and Dice Similarity Coefficient</h3>}
                    </div>
                </div>
            </div>
        );
    }

}

export default App;
